# bs2022

#### 介绍
nodejs+express+mongodb论坛接口



#### 安装教程

1.  git clone 或者下载zip解压
2.  进入文件目录npm install安装依赖
3.  node app.js启动项目

#### 使用说明

1.  config目录存放配置文件
2.  model的index为MongoDB数据库链接配置
3.  routers路由入口  controller控制器（业务逻辑） middleware中间件 model数据模型  public公共资源和文件上传位置 utils工具函数

#### 参与贡献

1.前端羽树 后端zzt


#### 特技

1.  瞎写写，甚至基本没脸说是自己写的

