const router = require('express').Router()
const avatar = require('../controller/info')
const {avatarValidator} = require('../model/avatar')
const validator = require('../middleware/validator')

//获取头像列表
router.get("/avatar", avatar.getUserAvatar)

router.get("/reply/expressions",avatar.getExpressions)
module.exports = router