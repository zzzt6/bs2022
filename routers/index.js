const router = require('express').Router()

//用户模块
router.use('/user',require('./user'))

//帖子模块
router.use('/topic',require('./topics'))

//文件上传
router.use('/upload',require('./upload'))

//登录
router.use('/auth',require('./auth'))

//获取用户信息
router.use('/info',require('./info'))

//回帖
router.use('/comment',require('./reply'))

//讨论
router.use('/discuss',require("./discussion"))

//聊天
let chat = require('./chat')
router.use('/chat',chat)
router.io = chat.io

//管理员
router.use('/admin',require('./admin'))

module.exports=router