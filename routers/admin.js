const router=require('express').Router()
const admin = require('../controller/admin')
router.post('/login',admin.login)
const validator = require('../middleware/validator')
const {userValidator} = require('../model/user')
const {topicValidator} = require('../model/topics')
const checkTopicExist = require('../middleware/checkTopicExist')

//获取用户列表
router.get('/userList', admin.getUserList)

//获取指定用户
router.get('/user/:id', admin.getUser)

//修改指定用户
//put与patch区别：put更新全部数据，patch只更新部分数据
router.patch('/user/:id', validator(userValidator), admin.updateUser)

//删除指定用户
router.delete('/user/:id', admin.delUser)

//获取帖子列表
router.get('/topicList', admin.getTopicList)

//获取待审核帖子列表
router.get('/uncheck/topicList',admin.getCheckTopics)
// 新增帖子
// router.post("/topic", admin.createTopic)

//修改贴子
router.patch("/topic/:id", [validator(topicValidator),checkTopicExist], admin.updateTopic)

//获取指定帖子
router.get("/topic/:id", checkTopicExist,admin.getTopic)

//删除帖子
router.delete("/topic/:id",checkTopicExist,admin.deleteTopic)

module.exports=router