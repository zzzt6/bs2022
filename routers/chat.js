const router = require('express').Router()
const chat = require('../controller/chat')
const auth = require("../middleware/auth")
//获取信息列表
router.get('/:id',auth,chat.getMessage)

//获取联系人列表/旧
router.get('/contact/list',auth,chat.getContactList)

//获取联系人列表/新
router.get('/mycontact/list',auth,chat.getUserContactList)

//移除联系人
router.delete("/contact/:id",auth,chat.deleteContact)
module.exports = router