const router = require('express').Router()
const auth = require('../controller/auth')
const {userValidator} = require('../model/user')
const validator = require('../middleware/validator')
//登录
router.post('/',validator(userValidator), auth.login)
//获取验证码
router.get('/sendcode',auth.sendCode)
//短信登录
router.post("/login/code", auth.loginByCode);
module.exports = router