const router = require('express').Router()
const discussion = require("../controller/discussion")
const auth = require("../middleware/auth")
const checkReplyExist = require('../middleware/checkReplyExist')
const checkReplier = require('../middleware/checkReplier')
//回复评论
router.post("/:id",auth,discussion.createDiscussion)

//回复二级评论
router.post("/reply/:id",auth,discussion.replyDiscussion)

//删除回复
router.delete("/:id",auth,discussion.deleteDiscussion)

//点赞回复
//点赞
router.put("/likingDiscuss/:id",[auth],discussion.likeDiscussion)

//取消点赞
router.delete("/unlikingDiscuss/:id",[auth],discussion.unLikeDiscussion)

module.exports=router