const router = require('express').Router()
const user = require('../controller/user')
const checkUserExists = require('../middleware/checkUserExist')
const auth =require('../middleware/auth')
const validator = require('../middleware/validator')
const checkToken = require('../middleware/checkToken')
const {userValidator} = require('../model/user')
const checkTopicExist = require('../middleware/checkTopicExist')

//注册
router.post('/',validator(userValidator),user.register)

//获取用户列表
router.get('/',user.getUserList)

//获取指定用户信息
router.get('/:id',checkUserExists,user.getUser)

//修改用户头像
router.put('/avatar/:id',auth,user.updateUserAvatar)

//修改用户信息
router.patch("/:id",[auth,validator(userValidator)],user.updateUser)

//修改登录用户信息
router.patch("/my/details",[auth,validator(userValidator)],user.updateLoginUser)

//根据token获取用户信息
router.get('/msg/token',auth,user.getUserByToken)

//关注
router.put("/following/:id", [auth,checkUserExists],user.follow)

//取消关注
router.delete("/unfollowing/:id", [auth,checkUserExists],user.unfollow)

//获取用户关注列表
router.get("/:id/followingList",checkUserExists, user.getFollowingList)

//获取用户的粉丝列表
router.get("/:id/fansList",checkUserExists,user.getFollowers)

//点赞帖子
router.put("/likingTopic/:id",[auth,checkTopicExist],user.likeTopic)

//取消点赞
router.delete("/unlikingTopic/:id",[auth,checkTopicExist],user.unLikeTopic)

//获取我点赞的帖子列表
router.get("/my/likingTopic",checkToken,user.getMyLikingTopics)

//收藏帖子
router.put("/collectingTopic/:id",[auth,checkTopicExist],user.collectingTopic)

//取消收藏
router.delete("/uncollectingTopic/:id",[auth,checkTopicExist],user.uncollectingTopic)

//获取指定用户收藏列表
router.get("/collectingTopic/:id",checkUserExists,user.collectingTopicList)

//获取我的收藏列表
router.get("/my/collectingTopic",auth,user.getMyCollectingTopics)

//获取我的点赞评论列表
router.get('/my/likingComments',auth,user.getMyLikingComments)
module.exports = router