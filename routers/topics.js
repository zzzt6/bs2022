const router = require('express').Router()
const topic = require('../controller/topic')
const validator = require('../middleware/validator')
const { topicValidator } = require('../model/topics')
const auth = require('../middleware/auth')
const checkTopicExist = require('../middleware/checkTopicExist')
const checkUserExist = require('../middleware/checkUserExist')
const checkToken = require('../middleware/checkToken')
//获取帖子列表
router.get('/',checkToken, topic.getTopicList)

//新增帖子
router.post("/",[auth,validator(topicValidator)], topic.createTopic)

//修改贴子
router.patch("/:id", [auth,validator(topicValidator),checkTopicExist], topic.updateTopic)

//获取指定帖子
router.get("/:id", checkTopicExist,topic.getTopic)

//删除帖子
router.delete("/:id",[auth,checkTopicExist],topic.deleteTopic)

//根据用户id获取帖子
router.get('/userTopics/:id',checkUserExist,topic.getUserTopic)

//帖子阅读+1
router.put("/read/:id",checkTopicExist,topic.readTopic)

//最早发的帖子在最上面
router.get("/time/asc",checkToken,topic.getTopicListbyTimeASC)

//点赞最多的帖子在上面
router.get('/praise/desc',checkToken,topic.getTopicListbypraise)

//获取热门推荐
router.get('/sort/hot',checkToken,topic.getTopicListbyHot)

module.exports = router