const router = require('express').Router()
const auth = require('../middleware/auth')
const { replyValidator, Reply } = require('../model/reply')
const validator = require('../middleware/validator')
//检测回复或用户是否存在
const checkReplyExist = require('../middleware/checkReplyExist')
const checkReplier = require('../middleware/checkReplier')
const checkTopicExist = require('../middleware/checkTopicExist')
const checkUserExist = require("../middleware/checkUserExist")
const reply = require('../controller/reply')
const checkToken = require("../middleware/checkToken")

//获取帖子回复列表
router.get('/:id',[checkTopicExist,checkToken],reply.getReplyList)

//获取指定回复
router.get('/one/:id',checkReplyExist,reply.getReply)

//回复
router.post('/:id',[auth,checkTopicExist,validator(replyValidator)],reply.createReply)

//更新/修改回复
// router.patch('/:id',[auth,validator(answerValidator),checkAnswerer,checkAnswerExist],answer.updateAnswer)

//删除回复
router.delete('/:id',[auth,checkReplyExist],reply.deleteReply)

//获取指定用户评论列表
router.get("/personal/:id",checkUserExist,reply.getUserReply)

//点赞
router.put("/likingReply/:id",[auth,checkReplyExist],reply.likeReply)

//取消点赞
router.delete("/unlikingReply/:id",[auth,checkReplyExist],reply.unLikeReply)

module.exports = router
