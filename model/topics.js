const mongoose = require('mongoose')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

//定义topic结构
const topicSchema = new mongoose.Schema({
    __v: {
        type: Number,
        select: false
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user"
    },
    title: {
        type: String,
        maxlength: 40
    },
    content: {
        type: String,
        maxlength: 10000,
    },
    text:{
        type: String,
        defauit: '',
        maxlength: 4000
    },
    imgArr:[{
        type:String,
        select: false
    }],
    images: {
        type: [{
            type:mongoose.Schema.Types.ObjectId,
            ref:"postImg"
        }]
    },
    videos: [{ type: String }],
    //帖子状态 0冻结，1正常
    status:{
        type:Number,
        select: false,
        default:0
    },
    //分类
    cate:{
        type: String,
        enum: ['综合', '攻略','资讯','话题'],
        default: '综合',
    },
    createdAt: {
        type: String,
        // default: moment().tz("Asia/Shanghai").format('YYYY/MM/DD HH:mm:ss')
    },
    updatedAt: {
        type: String
    },
    read:{
        type:Number,
        default: 0
    },
    comments:{
        type:Number,
        default: 0
    },
    praise:{
        type:Number,
        default: 0
    },
    collected: {
        type: Number,
        default: 0
    }
})
function topicValidator(data) {
    const schema = Joi.object({
        user:Joi.objectId(),
        title: Joi.string().max(40).required().messages({
            "any.required": "缺少必填项title",
            "string.base": "name的类型必须为string",
            "string.max": 'title最长为40字符'
        }),
        content: Joi.string().max(10000).messages({
            "string.base": "content的数据类型必须为string",
            'string.max':"正文content的长度不能超过10000"
        }),
        text:Joi.string().default("").max(4000).empty("").messages({
            "string.base":"base的类型必须为string",
            'string.max':"文字text的长度不能超过4000"
        }),
        imgArr: Joi.array().items(
            Joi.string().messages({
                "array.base": "imgArr必须为数组",
                "string.base":"imgArr的子项必须为string类型"
            })
        ),
        status:Joi.number().default(0),
        images: Joi.array().items(
            Joi.objectId()).messages({
            "array.base": "images必须为数组",
            "string.pattern.name": "images数组中的每个项必须为objectId类型"
        }),
        videos: Joi.array().items(
            Joi.objectId()).messages({
            "array.base": "videos必须为数组",
            "string.pattern.name": "location数组中的每个项必须为objectId类型"
        }),
        createdAt: Joi.date().messages({
            "string.base":"创建时间必须为number类型"
        }),
        updatedAt: Joi.date().messages({
            "string.base":"更新时间必须为number类型"
        }),
        read: Joi.number().default(0).messages({
            "number.base":"阅读数量必须为number类型"
        }),
        comments: Joi.number().default(0).messages({
            "number.base":"评论数量必须为number类型"
        }),
        praise: Joi.number().default(0).messages({
            "number.base":"点赞数量必须为number类型"
        }),
        collected:Joi.number().default(0).messages({
            "number.base":"收藏数量必须为number类型"
        }),
        cate: Joi.any().valid("综合", "攻略","资讯","话题").default('综合').messages({
            "any.only": "分类cate的值只能是综合、攻略、资讯、话题之间的一个"
        }),
    })
    return schema.validate(data)
}
//创建模型
const Topic = mongoose.model("Topic", topicSchema)

module.exports = {
    Topic,
    topicValidator
}