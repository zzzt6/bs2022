const mongoose = require('mongoose')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

//定义topic结构
const leverSchema = new mongoose.Schema({
    __v: {
        type: Number,
        select: false
    },
    level: {
        type: Number,
        require:true
    },
    url: {
        type: String,
        require:true
    },
    experience:{
        type: Number
    }
})
function leverValidator(data) {
    const schema = Joi.object({
        level: Joi.number().required().messages({
            "any.required": "缺少必填项lever",
            "string.base": "name的类型必须为number"
        }),
        url: Joi.string().required().messages({
            "any.required": "缺少必填项name",
            "string.base": "content的数据类型必须为string"
        }),
    })
    return schema.validate(data)
}
//创建模型
const Level = mongoose.model("Level", leverSchema)

module.exports = {
    Level,
    leverValidator
}