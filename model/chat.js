const mongoose = require('mongoose')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const moment = require('moment-timezone')
//定义topic结构
const chatSchema = new mongoose.Schema({
    __v: {
        type: Number,
        select: false
    },
    from: {
        type: mongoose.Schema.Types.ObjectId,
        ref:"user",
        require:true
    },
    to: {
        type: mongoose.Schema.Types.ObjectId,
        ref:"user",
        require:true
    },
    dateTime: {
        type: String,
        // require:true
    },
    message:{
        type:String,
        require:true
    }
})
function chatValidator(data) {
    const schema = Joi.object({
        from: Joi.objectId().required().messages({
            "any.required": "缺少必填项from",
            "string.pattern.base": "from格式错误"
        }),
        to: Joi.objectId().required().messages({
            "any.required": "缺少必填项name",
            "string.pattern.base": "to格式错误"
        }),
        dateTime: Joi.string().required().messages({
            "any.required": "缺少必填项dateTime",
            "string.base": "dateTime的类型必须为string"
        }),
        message: Joi.string().required().messages({
            "any.required": "缺少必填项message",
            "string.base": "message的数据类型必须为string"
        }),
    })
    return schema.validate(data)
}
//创建模型
const Chat = mongoose.model("Chat", chatSchema)

module.exports = {
    Chat,
    chatValidator
}