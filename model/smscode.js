const mongoose = require('mongoose')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

//定义topic结构
const smsCodeSchema = new mongoose.Schema({
    __v: {
        type: Number,
        select: false
    },
    //内容
    phone: {
        type: String,
        required: true,
        length:11
    },
    code:{
        type: String,
        required: true,
        length:6
    },
    createdAt: {
        type: String,
    }
    
})
function smsCodeValidator(data) {
    const schema = Joi.object({
        phone: Joi.string().required().length(11).messages({
            "string.base": 'phone必须是string类型',
            "any.required": "phone为必填项"
        }),
        code: Joi.string().length(6).messages({
            "string.base":'code必须是string类型',
            "any.required": "code为必填项"
        }),
    })
    return schema.validate(data)
}
//创建模型
const SMSCode = mongoose.model("SMSCode", smsCodeSchema)

module.exports = {
    SMSCode,
    smsCodeValidator
}