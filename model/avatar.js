const mongoose = require('mongoose')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

//定义topic结构
const avatarSchema = new mongoose.Schema({
    __v: {
        type: Number,
        select: false
    },
    name: {
        type: String,
        require:true
    },
    url: {
        type: String,
        require:true
    }
})
function avatarValidator(data) {
    const schema = Joi.object({
        name: Joi.string().required().messages({
            "any.required": "缺少必填项name",
            "string.base": "name的类型必须为string"
        }),
        url: Joi.string().required().messages({
            "any.required": "缺少必填项name",
            "string.base": "content的数据类型必须为string"
        }),
    })
    return schema.validate(data)
}
//创建模型
const Avatar = mongoose.model("Avatar", avatarSchema)

module.exports = {
    Avatar,
    avatarValidator
}