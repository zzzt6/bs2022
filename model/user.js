const config = require('../config')
const jwt = require('jsonwebtoken')
const mongoose = require('mongoose')
const joi = require('joi')
joi.objectId = require('joi-objectid')(joi)

//定义用户数据库user结构
const userSchema = new mongoose.Schema({
    //个人资料部分
    //邮箱
    phone: {
        type: String,
        required: true,
        length: 11,
        unique: true,
        select:false
    },
    //用户名
    nickname: {
        type: String,
        // required: true,
        minlength: 0,
        maxlength: 20,
        // unique: true
    },
    password: {
        type: String,
        required: true,
        minlength: 6,
        maxlength: 1000,
        select: false
    },
    //用户状态 0冻结，1正常
    status:{
        type:Number,
        select: false,
        default:1
    },
    //隐藏版本信息__v
    __v: {
        type: Number,
        select: false
    },
    //头像
    avatar: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Avatar',
        default: "6352d1f81d500d4c690b102a"
    },
    //性别
    gender: {
        type: String,
        enum: ['male', 'female'],
        default: 'male',
        select: false
    },
    //创建时间
    createdAt: {
        type: String,
        select: false
    },
    perLocation:{
        type: String,
        default:"未知IP"
    },
    //个人介绍
    headline: {
        type: String,
        default: "这货没有个人介绍"
    },
    praiseCount: {
        type: Number,
        default: 0,
        select: false
    },
    //经验值
    experience:{
        type: Number,
        default: 0,
        select: false
    },

    //聊天列表
    chatContact:{
        type: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: "user"
        }],
        select: false
    },
    //关注与粉丝部分
    //关注列表
    following: {
        type: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: "user"
        }],
        select: false
    },

    //帖子部分
    //收藏帖子
    followingTopic: {
        type: [{
            //相当于mysql中的外键
            type: mongoose.Schema.Types.ObjectId,
            ref: "Topic"
        }],
        select: false
    },
    //点赞的帖子
    likingTopics: {
        type: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: "Topic"
        }],
        select: false,
    },
    // dislikingAnswers: {
    //     type: [{
    //         type:mongoose.Schema.Types.ObjectId,
    //         ref: "Answer"
    //     }],
    //     select: false,
    // },
    //点赞评论
    likingReplies: {
        type: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: "Reply"
        }],
        select: false,
    },
    //收藏帖子
    collectingTopics: {
        type: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: "Topic"
        }],
        select: false,
    }
})


//封装生成token的功能
userSchema.methods.generateToken = function () {
    return jwt.sign({
        _id: this._id
    }, config.secret, { expiresIn: '7day' })
}
//引入用户模块
const User = mongoose.model("user", userSchema)

function userValidator(data) {
    const schema = joi.object({
        phone: joi.string().trim().length(11).pattern(/^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/).messages({
            "string.length": "phone的长度须为11字符",
            "string.base": "phone的类型必须是string",
            "string.pattern.base": "phone格式错误"
        }),
        nickname: joi.string().min(2).max(12).messages({
            "any.required": "缺少必选的参数nickname",
            "string.base": "nickname必须为String类型",
            "string.min": "nickname最少为2字符",
            "string.max": "nickname最多为14字符"
        }),
        password: joi.string().pattern(/^[a-zA-Z0-9!@.?]{6,16}$/).messages({
            "string.base": "password必须为String类型",
            "string.min": "password最少为6字符",
            "string.max": "password最多为16字符",
            "string.pattern.base": "密码格式错误,包含大小写英文数字和!@.?"
        }),
        _id: joi.objectId,
        status:joi.number().default(1).messages({
            "number.base": "password必须为Numebr类型",
        }),
        avatar: joi.objectId().messages({
            "string.base": '图片地址必须为string类型'
        }),
        gender: joi.any().valid("male", "female").messages({
            "any.only": "gender只能传入male或female"
        }),
        createdAt: joi.date().messages({
            "string.base": '创建时间必须为string类型'
        }),
        headline: joi.string().max(100).messages({
            'string.base': "headerline必须为string类型",
            "string.max": 'headline最长为100字符'
        }),
        following: joi.array().items(
            joi.object().keys({
                type: joi.objectId()
            })
        ).messages({
            "array.base": "following必须为数组类型"
        }),
        followingTopic: joi.array().items(
            joi.object().keys({
                type: joi.objectId()
            })
        ).messages({
            "array.base": "followingTopic必须为数组类型"
        }),
        likingTopics: joi.array().items(joi.objectId()).messages({
            "array.base": "likingTopics必须为数组",
            "string.pattern.name": "likingTopics数组中的每个项必须为objectId类型"
        }),
        collectingTopics: joi.array().items(joi.objectId()).messages({
            "array.base": "collectingTopics必须为数组",
            "string.pattern.name": "collectingTopics数组中的每个项必须为objectId类型"
        }),
        likingReplies: joi.array().items(joi.objectId()).messages({
            "array.base": "likingReplies必须为数组",
            "string.pattern.name": "likingReplies数组中的每个项必须为objectId类型"
        }),
    })
    return schema.validate(data)
}

module.exports = {
    User,
    userValidator
}