const mongoose = require('mongoose')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

//定义topic结构
const replySchema = new mongoose.Schema({
    __v: {
        type: Number,
        select: false
    },
    //内容
    content: {
        type: String,
        required: true
    },
    //评论人
    replier: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true,
    },
    //评论的帖子id
    topicId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Topic'
    },
    //评论的点赞数量
    voteCount: {
        type: Number,
        default: 0
    },
    //你回复的评论id
    rootCommentId: {
        type: String
    },
    // 你要回复的人
    replyTo: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    createdAt: {
        type: String
    },
    //子评论
    childrenComments: {
        type: [{  }],
        default: [],
        select: false,
    }
})
function replyValidator(data) {
    const schema = Joi.object({
        content: Joi.string().required().messages({
            "string.base": 'content必须是string类型',
            "any.required": "content为必填项"
        }),
        replier: Joi.objectId().messages({
            // "string.base":'answerer必须是string类型',
            "any.required": "answerer为必填项"
        }),
        topicId: Joi.string().messages({
            "string.base": "questionId必须是string类型"
        }),
        voteCount: Joi.number().messages({
            "number.base": "voteCount必须是number类型"
        }),
        rootCommentId: Joi.number().messages({
            "string.base": "voteCount必须是string类型"
        }),
        replyTo: Joi.objectId(),
        createdAt: Joi.string().messages({
            "string.base": "createdAt必须是string类型"
        }),
        childrenComments: Joi.array()
    })
    return schema.validate(data)
}
//创建模型
const Reply = mongoose.model("Reply", replySchema)

module.exports = {
    Reply,
    replyValidator
}