const express = require('express')
const app = express()
const cors = require('cors')
const morgan =require('morgan')
const config = require('./config')
//导入http模块并生成socket.io
const http = require('http')
const server = http.createServer(app)
require('./utils/timer')
const io = require('./utils/socketio')

//解析json数据结构
app.use(express.json())
//解析表单数据结构
app.use(express.urlencoded({ extended: false }))
//静态资源托管
app.use(express.static('public'))
//跨域
app.use(cors())
//日志
app.use(morgan('dev'))
//引入数据库
require('./model')
//引入路由并传入io
const routers= require('./routers/index')
app.use('/api',routers)
// app.io = routers.io
// app.io(io)
//引入错误中间件
app.use(require('./middleware/error'))
server.listen(config.app.port,()=>{
    console.log(`server listening on port http://localhost:${config.app.port}`)
})
io.getSocketio(server)
