const config = require('../config');
const { postImg } = require('../model/postImg')
const {rootPath} = require('../utils/index')
exports.uploadFile = async (req, res, next) => {
    try {
        // let topicId = req.params.id
        let userId = ''
        if(req.userData._id){
            userId = req.userData._id
        }
        // console.log(userId)
        const topicImages = new postImg({ 
            url: `/uploads/${req.userData._id}/` + req.file.filename,
            name:req.file.filename ,
            // topic: topicId
            user:userId
        })
        await topicImages.save()
        // req.uploadImages=topicImages._id
        res.status(200).json({
            errno: 0,
            code: 200,
            msg: '文件上传成功',
            data: {
                // url: `${config.server}:${config.app.port}` + `/uploads/${req.userData._id}/` + req.file.filename,
                url: "http://localhost:8007"+ `/uploads/${req.userData._id}/` + req.file.filename,
                
                alt: req.file.filename,                                 
            }
        })
    } catch (err) {
        next(err)
    }
}
