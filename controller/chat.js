const { Chat } = require('../model/chat')
const {User} = require("../model/user")
exports.getMessage = async (req, res, next) => {
    try {
        const myId = req.userData._id
        const talkId = req.params.id
        const msgList1 = await Chat.find({
            from: myId,
            to: talkId
        }).populate({
            path: "from to",
            strictPopulate: false,
            populate: {
                path: "avatar",
                strictPopulate: false
            }
        })
        const msgList2 = await Chat.find({
            from: talkId,
            to: myId
        }).populate({
            path: "from to",
            strictPopulate: false,
            populate: {
                path: "avatar",
                strictPopulate: false
            }
        })
        let msgList= [...msgList1,...msgList2]
  
        for(var i=0; i<msgList.length-1; i++){
        //每一轮比较要比多少次
            for(var j=0; j<msgList.length-1-i; j++){
                //如果第一个比第二个大，就交换他们两个位置
                if(msgList[j]>msgList[j+1]){
                    var temp = msgList[j];
                    msgList[j] = msgList[j+1];
                    msgList[j+1] = temp;
                }
            }    
        }

        
        if(!msgList) return res.status(200).json({
            code:400,
            msg:"获取聊天信息列表失败"
        })
        res.status(200).json({
            code: 200,
            msg: "获取聊天信息列表成功",
            data: msgList
        })
    } catch (err) {
        next(err)
    }
}
exports.getContactList = async (req, res, next) => {
    try {
        if (!req.userData._id) return res.status(400).json({
            code: 400,
            msg: "请先登录"
        })
        const messageList = await Chat.find({
            from: req.userData._id
        }).populate({
            path: "from to",
            strictPopulate: false,
            populate: {
                path: "avatar",
                strictPopulate: false
            }
        })
        if (!messageList) return res.status(200).json({
            code: 400,
            msg: "获取联系人列表失败"
        })
        let contactList = []
        messageList.forEach((item, index, array) => {
            if (!contactList.includes(item.to)) {
                contactList.push(item.to)
            }
        })
        res.status(200).json({
            code: 200,
            msg: "获取联系人列表成功",
            data: contactList
        })
    } catch (err) {
        next(err)
    }
}
exports.getUserContactList = async (req,res,next) => {
    try{
        // const messageList = await Chat.find({from: req.userData._id}).populate({
        //     path: "from to",
        //     strictPopulate: false,
        //     populate: {
        //         path: "avatar",
        //         strictPopulate: false
        //     }
        // })
        // let contactsList = []
        // messageList.forEach((item, index, array) => {
        //     if (!contactsList.includes(item.to)) {
        //         contactsList.push(item.to)
        //     }
        // })
        //新
        const userId = req.userData._id
        const contactList = await User.findById(userId).select("+chatContact").populate({
            path:"chatContact",
            populate:{
                path:"avatar",
                strictPopulate:false
            }
        })
        if (!contactList) return res.status(200).json({
            code: 400,
            msg: "获取联系人列表失败"
        })
        res.status(200).json({
            code: 200,
            msg: "获取联系人列表成功",
            data: contactList.chatContact
        })
    } catch (err){
        next(err)
    }
}
exports.deleteContact = async (req, res, next) => {
    try {
        const loginuser = await User.findById(req.userData._id).select("+chatContact")
        let contactList = loginuser.chatContact
        if(!contactList.includes(req.params.id)) return res.status(200).json({
            code: 400,
            msg:"移除失败，该用户不在你的联系人列表中"
        })
        const remove = await User.findByIdAndUpdate(req.userData._id,{
            $pull:{
                chatContact:req.params.id
            }
        })
        
        if(!remove) return res.status(200).json({
            code:400,
            msg:"移除联系人失败"
        })
        const mycontactList = await User.findById(req.userData._id).select("+chatContact").populate({
            path:"chatContact",
            populate:{
                path:"avatar",
                strictPopulate:false
            }
        })
        res.status(200).json({
            code:200,
            msg:"移除联系人成功",
            data:mycontactList.chatContact
        })
    } catch (err) {
        next(err)
    }
}