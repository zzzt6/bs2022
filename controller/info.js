const {Avatar} =require('../model/avatar')
const {Expression} = require('../model/expressions')
//获取用户头像列表
exports.getUserAvatar = async (req, res, next) => {
    try{
        const avatar = await Avatar.find()

        if(!avatar) return res.status(200).json({
            code:400,
            msg:'获取头像列表失败'
        })
        res.status(200).json({
            code:200,
            msg:'获取头像列表成功',
            data:avatar
        })
    } catch(err){
        next(err)
    }
}
exports.getExpressions = async (req, res, next) => {
    try{
        const expressions = await Expression.find()

        if(!expressions) return res.status(200).json({
            code:400,
            msg:'获取表情列表失败'
        })
        res.status(200).json({
            code:200,
            msg:'获取头像列表成功',
            data:expressions
        })
    } catch(err){
        next(err)
    }
}