const {Topic} = require('../model/topics')
module.exports = async (req,res,next) => {
    let pid = req.params.id
    if (pid.match(/^[0-9a-fA-F]{24}$/)){
        const topic = await Topic.findById(pid)
        if(!topic) return res.status(200).json({
            code: 404,
            msg:'帖子不存在'
        })
    }
    else {
        res.status(200).json({
            code:400,
            msg:'帖子不存在'
        })
    }
    next()
}