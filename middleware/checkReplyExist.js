const {Reply} = require('../model/reply')
module.exports = async (req, res, next) => {
    let pid = req.params.id
    if (pid.match(/^[0-9a-fA-F]{24}$/)){
        const reply = await Reply.findById(pid).select("+replier")
        if(!reply) return res.status(404).json({
            code: 404,
            msg:'这条回复不存在'
        })
        // if(reply.topicId !== req.params.id){
        //     return res.status(200).json({
        //         code: 404,
        //         msg:'这个帖子没有回帖'
        //     })
        // }
    }
    else {
        res.status(200).json({
            code:400,
            msg:'这条回复不存在'
        })
    }
    next()
}