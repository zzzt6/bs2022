//数据格式验证错误中间件，如果有error则返回相应的数据
module.exports = (validator) =>{
    return (req,res,next) =>{
        const {error,value} = validator(req.body)
        if(error){
            return res.status(200).json({
                code:400,
                //数据源
                data:error._original,
                //错误信息
                msg:error.details[0].message
            })
        }
        req.validValue = value
        next()
    }
}