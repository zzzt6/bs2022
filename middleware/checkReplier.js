const { Reply } = require('../model/reply')
module.exports = async (req, res, next) => {
    let pid = req.params.id
    if (pid.match(/^[0-9a-fA-F]{24}$/)) {
        const reply = await Reply.findById(pid).select("+replier")
        if (reply.replier.toString() !== req.userData._id) {
            return res.status(404).json({
                code: 404,
                msg: '请先登录'
            })
        }
    }
    else {
        res.status(200).json({
            code: 400,
            msg: '请先登录'
        })
    }
    next()
}