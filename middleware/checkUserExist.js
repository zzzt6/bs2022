const {User} = require('../model/user')
module.exports = async (req,res,next) => {
    let pid = req.params.id
    if (pid.match(/^[0-9a-fA-F]{24}$/)){
        const user = await User.findById(pid)
        if(!user) return res.status(200).json({
            code: 404,
            msg:'用户不存在'
        })
    }
    else {
        res.status(200).json({
            code:400,
            msg:'用户不存在'
        })
    }
    next()
}